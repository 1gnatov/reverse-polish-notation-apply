package main

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

func TestAdd(t *testing.T) {
	a := 5.0
	b := 3.3
	var operation BiConsumer = Add{}
	result, _ := operation.Apply(a, b)
	if result != 8.3 {
		panic("")
	}
}

func TestMinus(t *testing.T) {
	a := 5.0
	b := 3.3
	var operation BiConsumer = Minus{}
	result, _ := operation.Apply(a, b)
	if result-1.7 > 0.00001 {
		panic("")
	}
}

func TestMultiply(t *testing.T) {
	a := 5.0
	b := 3.3
	var operation BiConsumer = Multiply{}
	result, _ := operation.Apply(a, b)
	if result != 16.5 {
		panic("")
	}
}

func TestDivide(t *testing.T) {
	a := 6.6
	b := 3.3
	var operation BiConsumer = Divide{}
	result, _ := operation.Apply(a, b)
	if result != 2 {
		panic("")
	}
}

func TestSin(t *testing.T) {
	a := 5.
	var operation Consumer = Sin{}
	result, _ := operation.Apply(a)
	if result-0.0871 > 0.000001 {
		panic("")
	}
}

func TestCos(t *testing.T) {
	a := 5.
	var operation Consumer = Cos{}
	result, _ := operation.Apply(a)
	if result-0.9961 > 0.000001 {
		panic("")
	}
}

func TestTan(t *testing.T) {
	a := 5.
	var operation Consumer = Tan{}
	result, _ := operation.Apply(a)
	if result-0.08748 > 0.000001 {
		panic("")
	}
}

func TestPow(t *testing.T) {
	a := 5.0
	b := 2.
	var operation BiConsumer = Pow{}
	result, _ := operation.Apply(a, b)
	if result != 25 {
		panic("")
	}
}

func TestCalculateAdd(t *testing.T) {
	rpnSlice := []string{"-10", "2", "+"}
	res := Calculate(rpnSlice, 0.)
	if res != -8 {
		panic("")
	}
}

func TestTabulate(t *testing.T) {
	xMin, xMax, h := 1., 3., 0.5
	res := Tabulate(xMin, xMax, h)
	if !reflect.DeepEqual([]float64{1., 1.5, 2., 2.5, 3}, res) {
		panic("")
	}
}

func TestTabulate2(t *testing.T) {
	xMin, xMax, h := 1., 3., 0.3
	res := Tabulate(xMin, xMax, h)
	exp := []float64{1, 1.3, 1.6, 1.9, 2.2, 2.5, 2.8, 3}
	for i := range res {
		if math.Abs(res[i]-exp[i]) > 0.001 {
			panic("")
		}
	}
}

func TestTwentyMultiplyX(t *testing.T) {
	rpn := []string{"20", "x", "*"}
	xMin, xMax, h := 1., 3., 0.5
	xArray := Tabulate(xMin, xMax, h)
	yArray := make([]float64, 0)

	for _, x := range xArray {
		res := Calculate(rpn, x)
		yArray = append(yArray, res)
	}
	fmt.Printf("\tx\t\t\tf(x)\n")
	printResult(xArray, yArray)
}
