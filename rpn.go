package main

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

type BiConsumer interface {
	Apply(a, b float64) (float64, error)
}

type Consumer interface {
	Apply(a float64) (float64, error)
}

// Add
type Add struct {
}

func (obj Add) Apply(a, b float64) (float64, error) {
	return a + b, nil
}

// Add
// Divide
type Divide struct {
}

func (obj Divide) Apply(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("can`t divide to zero")
	}
	return a / b, nil
}

// Divide
// Minus
type Minus struct {
}

func (obj Minus) Apply(a, b float64) (float64, error) {
	return a - b, nil
}

// Minus
// Multiply
type Multiply struct {
}

func (obj Multiply) Apply(a, b float64) (float64, error) {
	return a * b, nil
}
// Multiply
// Sin
type Sin struct {
}

func (obj Sin) Apply(a float64) (float64, error) {
	return math.Sin(a), nil
}
// Sin
// Cos
type Cos struct {
}

func (obj Cos) Apply(a float64) (float64, error) {
	return math.Cos(a), nil
}
// Cos
// Tan
type Tan struct {
}

func (obj Tan) Apply(a float64) (float64, error) {
	return math.Tan(a), nil
}
// Tan
// Pow
type Pow struct {
}

func (obj Pow) Apply(a, b float64) (float64, error) {
	return math.Pow(a, b), nil
}
// Pow

func parseOperation(s string) interface{} {
	switch s {
	case "+":
		return Add{}
	case "/":
		return Divide{}
	case "-":
		return Minus{}
	case "*":
		return Multiply{}
	case "sin":
		return Sin{}
	case "cos":
		return Cos{}
	case "tg":
		return Tan{}
	case "**":
		return Pow{}
	default:
		return nil
	}
}

func popLastElement(slice *[]float64) float64 {
	slc := *slice
	lastElem := slc[len(slc)-1]
	slc = slc[:len(slc)-1]
	*slice = slc
	return lastElem
}

func appendLastElement(slice *[]float64, fl float64) {
	slc := *slice
	slc = append(slc, fl)
	*slice = slc
}

// Calculate function in reverse polish notation. Do not check function to be correct.
func Calculate(rpn []string, x float64) float64 {
	buffer := make([]float64, 0)
	for _, elem := range rpn {
		if number, err := strconv.ParseFloat(elem, 32); err == nil { // parse number
			buffer = append(buffer, number)
		} else if strings.ToLower(elem) == "x" { // parse argument
			buffer = append(buffer, x)
		} else { // parse operation
			operation := parseOperation(elem)
			if operation != nil {
				switch action := operation.(type) {
				case BiConsumer:
					b := popLastElement(&buffer)
					a := popLastElement(&buffer)
					res, _ := action.Apply(a, b)
					appendLastElement(&buffer, res)
				case Consumer:
					a := popLastElement(&buffer)
					res, _ := action.Apply(a)
					appendLastElement(&buffer, res)
				}
			} else {
				panic("Operation is nil")
			}
		}
	}
	return popLastElement(&buffer)
}

func Tabulate(xMin, xMax, step float64) []float64 {
	if xMax == xMin {
		return []float64{xMax}
	}
	result := make([]float64, 0)[:]
	for i:=0; i <= int((xMax-xMin-step/2.)/step); i++ { // -step/2. is a hack to do not get double when append xMax 
		result = append(result, xMin + float64(i)*step)
	}
	result = append(result, xMax)
	return result
}

func printResult(xArray, yArray []float64) {
	for i := range xArray {
		fmt.Printf("\t%f\t%f\n", xArray[i], yArray[i])
	}
}

func main() {

	xArray := Tabulate(1., 1., 0.5)
	rpnSlice := []string{"-10", "2", "3", "6", "5", "/", "-", "1", "+", "*", "20", "/", "+", "1", "*", "1", "-"}
	yArray := make([]float64, 0)

	for _, x := range xArray {
		res := Calculate(rpnSlice, x)
		yArray = append(yArray, res)
	}
	printResult(xArray, yArray)
}
